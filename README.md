# Advanced Git Workshop
Lab 08: Checking out multiple branches simultaneously

---

# Tasks

 - Making some changes
 
 - Creating a worktree
 
 - Managing worktrees

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab8
$ cd lab8
```

---

## Making some changes

  - Let's create a new branch to work with:
```
$ git checkout -b feature/my-work
```

  - Let's make some changes to the **index.html**, change the title in the head section:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Demo App New Title</title>
```

---

## Creating a worktree

  - One moment, we were asked to fix a master bug in parallel, let's create a worktree for that:
```
$ git worktree add master-worktree master
$ cd master-worktree
```

  - Once ready let's work on the master-worktree, edit the head description in **index.html** and commit the changes:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="MY NEW PAGE DESCRIPTION">
    <meta name="author" content="">
```
```
$ git add index.html
$ git commit -m "add page description"
```

  - Now, come back to the main worktree and check the status:
```
$ cd ..
$ git status
```

 - We created the worktree within the repository, if we are not careful we could add it by mistake...

```
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        master-worktree/
```

---

## Managing worktrees

  - Let's move the worktree out of the repository:
```
$ git worktree move master-worktree ./../master-worktree
```

  - If we can review the repository worktrees we can use the command below:
```
$ git worktree list
```

  - After finishing using it we can remove the linked working tree with:
```
$ git worktree remove ../master-worktree
```

